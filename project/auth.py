import datetime
import re
import sqlite3
from sqlite3 import IntegrityError

import requests
from flask import Flask, render_template, request, redirect, url_for, flash, session

import config

app = Flask(__name__)
app.secret_key = config.SEC_KEY
db_path = "static/warehouse.db"
api = config.API_KEY


@app.route('/')
def welcome():
    return render_template("welcome.html")


@app.route("/register", methods=['Get', 'POST'])
def register():
    db = sqlite3.connect(db_path)
    c = db.cursor()
    if request.method == 'POST':
        username = request.form['username']
        email = request.form['email']
        password = request.form['password']
        email_pattern = r"[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}"
        is_valid = re.match(email_pattern, email)

        if not email or not username or not password:
            err_msg = "Please fill in all fields"
        elif not is_valid:
            err_msg = "Invalid Email Address"
        elif not re.search(r"^(?=.*[A-Z])", password):
            err_msg = "Password must contain an Uppercase letter"
        else:
            try:
                c.execute("INSERT INTO User_auth (email, username, password) VALUES (?, ?, ?)",
                          (email, username, password))
                db.commit()
                db.close()
                flash("Registration successful. Redirecting to login page in 3 seconds.")
                return redirect(url_for('register'))
            except IntegrityError:
                err_msg = "This email has been registered already."
        return render_template('auth/register.html', err_msg=err_msg)
    return render_template('auth/register.html', err_msg=None)


def check_user(email, password):
    db = sqlite3.connect(db_path)
    c = db.cursor()
    # Check if the user exists
    c.execute("SELECT password FROM user_auth WHERE email=?", (email,))
    result = c.fetchone()
    db.close()
    if result is None:
        return "User does not exist"
    # print(result)
    # Check if the password is correct
    if result[0] != password:
        return "Incorrect password"
    # If everything checks out, return None (no error)
    return None


@app.route("/login", methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        email = request.form["email"]
        password = request.form["password"]
        session['email'] = email
        err_msg = check_user(email, password)
        if err_msg:
            return render_template("auth/login.html", err_msg=err_msg)
        return redirect(url_for('finance_home'))
    return render_template('auth/login.html', err_msg=None)


# 存在问题：如果search的公司名不存在返回空时，应转到新页面“对不起，没有该symbol”
# 可优化问题，使用公司名称找symbol但是没必要
@app.route("/finance_home", methods=['GET', 'POST'])
def finance_home():
    if request.method == 'POST':
        symbol = request.form['search_content']
        return redirect(url_for('company_info', symbol=symbol))
    init_user_db()
    spy_data = get_stock_data(symbol='SPY')
    print(spy_data)
    email = session.get('email')
    # 查找username
    db = sqlite3.connect(db_path)
    c = db.cursor()
    c.execute("SELECT username FROM user_auth WHERE email=?", (email,))
    result = c.fetchone()
    session['username'] = result[0]
    c.execute(f'''SELECT * FROM [{email}]''')
    rows = c.fetchall()
    portfolio = {}
    for row in rows:
        symbol = row[0]
        volume = row[4]
        price = row[3]
        if symbol not in portfolio and symbol != 'Initial Balance':
            portfolio[symbol] = {'total_volume': 0, 'total_value': 0}
        if symbol != 'Initial Balance':
            portfolio[symbol]['total_volume'] += volume
            portfolio[symbol]['total_value'] += volume * price
    total_revenue = 0
    for symbol in portfolio.keys():  # 当portfolio为空会跳过这段运行-> 运气好绕过一个检测
        url = 'https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=' + symbol + '&interval=5min&apikey=' + api
        r = requests.get(url)
        data = r.json()
        latest_time = data['Meta Data']['3. Last Refreshed']
        cur_price = float(data['Time Series (5min)'][latest_time]['4. close'])
        portfolio[symbol]['current_price'] = cur_price
        portfolio[symbol]['revenue'] = round(
            (cur_price * portfolio[symbol]['total_volume'] - portfolio[symbol]['total_value']), 2)
        total_revenue += portfolio[symbol]['revenue']
    db.close()
    # print(portfolio)
    return render_template('post/finance_home.html', username=session.get('username'), data=rows, portfolio=portfolio,
                           total_revenue=round(total_revenue, 2), stock_data=spy_data, symbol="SPY")


def init_user_db():
    email = session.get('email')
    db = sqlite3.connect(db_path)
    c = db.cursor()
    c.execute(f'''CREATE TABLE IF NOT EXISTS [{email}]
                 (symbol TEXT NOT NULL,
                  date DATETIME NOT NULL,
                  trade_type TEXT NOT NULL,
                  price FLOAT NOT NULL,
                  volume INTEGER NOT NULL,

                  balance FLOAT NOT NULL)''')
    c.execute(f'SELECT * FROM [{email}]')
    if c.fetchone() is None:
        now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        initial_data = ('Initial Balance', now, 'Initial Balance', 0, 0, 10 ** 6)
        c.execute(f"INSERT INTO [{email}] VALUES (?, ?, ?, ?, ?,  ?)", initial_data)
        db.commit()
        print("Table" + email + " created and initial data inserted.")
    else:
        print("Table" + email + " already exists.")
    db.close()
    return None


@app.route("/company_info/<symbol>", methods=['GET', 'POST'])
def company_info(symbol):
    try:
        profile = get_profile(symbol, api)
        price = get_price(symbol, api)
    except KeyError:
        flash("Invalid stock symbol. Please try again.", "error")
        return redirect(url_for('finance_home'))
    db = sqlite3.connect(db_path)
    c = db.cursor()
    email = session.get('email')
    if request.method == 'POST':
        trade_type = request.form['buy_sell_select']
        trade_volume = int(request.form['trade_vol'])
        symbol = request.form['symbol']
        balance = float(request.form['current_balance'])
        order_price = float(request.form['price'])
        now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        if trade_type == "Buy":
            trade_volume = int(trade_volume) * 1
        elif trade_type == "Sell":
            trade_volume = int(trade_volume) * (-1)
        balance_change = float(order_price) * trade_volume
        post_trade_balance = round((balance - balance_change), 2)
        # print(post_trade_balance)
        c.execute(f'''INSERT INTO [{email}] ( symbol,date, trade_type, price, volume, 
           balance) VALUES (?, ?, ?, ?, ?, ?) ''',
                  (symbol, now, trade_type, order_price, trade_volume, post_trade_balance))
        db.commit()
        db.close()
        return redirect(url_for('company_info', symbol=symbol))
    stock_data = get_stock_data(symbol)
    profile = get_profile(symbol, api)
    symbol = profile['Symbol']
    # print(symbol)
    price = get_price(symbol, api)
    # print(price)
    c.execute(f'''SELECT balance FROM [{email}] ORDER BY date DESC LIMIT 1''')
    balance = c.fetchone()[0]
    c.execute(f'''SELECT volume FROM "{email}" WHERE symbol=?''', (symbol,))
    results = c.fetchall()
    if len(results) == 0:
        cur_vol = 0
    else:
        cur_vol = sum([result[0] for result in results])

    db.close()
    return render_template('search/company_info.html', stock_data=stock_data, symbol=symbol, profile=profile,
                           price=price, balance=balance, cur_vol=cur_vol)


def get_profile(symbol, api):
    profile = {}
    url = 'https://www.alphavantage.co/query?function=OVERVIEW&symbol=' + symbol + '&apikey=' + api
    r = requests.get(url)
    data = r.json()
    for key in data.keys():
        if key not in profile.keys():
            if key == "Symbol" or key == "Name" or key == "Description" or key == "Exchange" or key == "Sector" or key == "Industry" or key == "MarketCapitalization" or key == "PERatio" or key == "DividendPerShare" or key == "DividendYield" or key == "52WeekHigh" or key == "52WeekLow":
                profile[key] = data[key]
    # print(profile)
    return profile


# '5. adjusted close': '132.06', '6. volume': '3834689'
def get_price(symbol, api):
    price = {}
    url = 'https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol=' + symbol + '&apikey=' + api
    r = requests.get(url)
    data = r.json()
    # print(data)  # data存在日期有时候2023-04-03 or 2023-04-03 16:00:00
    latest_day = data['Meta Data']['3. Last Refreshed'][0:10]
    # print(latest_day)
    price['date'] = latest_day
    price['previous_open'] = data['Time Series (Daily)'][latest_day]['1. open']
    price['previous_close'] = data['Time Series (Daily)'][latest_day]['5. adjusted close']
    price['previous_vol'] = data['Time Series (Daily)'][latest_day]['6. volume']
    # print(price)
    url = 'https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=' + symbol + '&interval=5min&apikey=' + api
    r = requests.get(url)
    data = r.json()
    # print(data)
    latest_time = data['Meta Data']['3. Last Refreshed']
    # print('1123')
    # print(latest_time)
    current_time = datetime.datetime.strptime(latest_time, '%Y-%m-%d %H:%M:%S')
    price['time'] = latest_time

    price['current_price'] = float(data['Time Series (5min)'][latest_time]['4. close'])
    previous_time = min((x for x in data['Time Series (5min)'].keys() if x != latest_time),
                        key=lambda x: abs(datetime.datetime.strptime(x, '%Y-%m-%d %H:%M:%S') - current_time))
    # 使用这个方法可以提高查询速度
    previous_price = "{:.2f}".format(float(data['Time Series (5min)'][previous_time]['4. close']))
    # print(previous_price)
    price['price_change'] = "{:.2f}".format(float(price['current_price']) - float(previous_price))
    price['per_change'] = "{:.2f}".format((float(price['price_change']) / float(previous_price)) * 100)
    # print(price)
    return price


def get_stock_data(symbol):
    fetch_stock_data(api, symbol)
    db = sqlite3.connect(db_path)
    c = db.cursor()
    symbol = symbol.upper()
    if symbol == 'SPY':
        c.execute(f"SELECT date, adj_close FROM Historical_{symbol}_Data ORDER BY date")
    else:
        c.execute(f"SELECT date, adj_close FROM Stock_{symbol}_Data ORDER BY date")
    results = c.fetchall()
    stock_data = [{'date': row[0], 'close': row[1]} for row in results]
    db.close()
    return stock_data


def fetch_stock_data(api, symbol):
    symbol = symbol.upper()
    if symbol == 'SPY':
        table_name = "Historical_SPY_Data"
    else:
        table_name = f"Stock_{symbol}_Data"
    db = sqlite3.connect(db_path)
    c = db.cursor()
    c.execute(f'''CREATE TABLE IF NOT EXISTS {table_name} (
                date DATETIME PRIMARY KEY,
                open NUMERIC,
                high NUMERIC,
                low NUMERIC,
                close NUMERIC,
                adj_close NUMERIC,
                volume INTEGER
            )
        ''')
    c.execute(f'SELECT * FROM {table_name}')
    if c.fetchone() is None:
        url = f"https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol={symbol}&outputsize=full&apikey={api}"
        r = requests.get(url)
        data = r.json()
        historical_data = data["Time Series (Daily)"]
        latest_day = datetime.datetime.strptime(data['Meta Data']['3. Last Refreshed'][0:10], '%Y-%m-%d')
        five_years_ago = latest_day - datetime.timedelta(days=5 * 365)
        for date, data in historical_data.items():
            dt = datetime.datetime.strptime(date[0:10], '%Y-%m-%d')
            if dt >= five_years_ago:
                open_price = data['1. open']
                high_price = data['2. high']
                low_price = data['3. low']
                close_price = data['4. close']
                adj_close = data['5. adjusted close']
                volume = data['6. volume']

                c.execute(f'''
                    INSERT INTO {table_name} (date, open, high, low, close, adj_close, volume)
                    VALUES (?, ?, ?, ?, ?, ?, ?)
                ''', (date[0:10], open_price, high_price, low_price, close_price, adj_close, volume))
        db.commit()
    else:
        c.execute(f"SELECT date FROM {table_name} ORDER BY date DESC LIMIT 1")
        result = c.fetchone()
        url = f"https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol={symbol}&outputsize=full&apikey={api}"
        r = requests.get(url)
        data = r.json()
        # print(data['Meta Data']['3. Last Refreshed'])
        latest_day = (data['Meta Data']['3. Last Refreshed'])[0:10]
        # print(latest_day)
        if result[0] != latest_day:
            info = data['Time Series (Daily)'][latest_day]
            c.execute(f'''
                               INSERT INTO {table_name} (date, open, high, low, close, adj_close, volume)
                               VALUES (?, ?, ?, ?, ?, ?, ?)
                           ''', (
                latest_day, info['1. open'], info['2. high'], info['3. low'], info['4. close'],
                info['5. adjusted close'],
                info['6. volume']))
            db.commit()
    db.close()
    return None


if __name__ == '__main__':
    app.run(debug=True)
