var ready = (callback) => {
    if (document.readyState != "loading") callback();
    else document.addEventListener("DOMContentLoaded", callback);
  }
  
  ready(() => { 
    document.getElementById("Submit-button").addEventListener('click',whenclick)
  });

function whenclick(event){
  console.log(event);
  bootbox.confirm("Do you want to submit the form?", function(result){
    if (result){
      bootbox.alert("Form submitted");
      makeplot();
    } else{
      bootbox.alert("Cancelled");
    }
  });

};

function makeplot() {
    console.log("makeplot: start")
    var select = document.getElementById("stock-select");
    var stock = select.value;
    fetch("/static/" + stock + ".csv")
    .then((response) => response.text()) /* asynchronous */
    .catch(error => {
        alert(error)
         })
    .then((text) => {
      console.log("csv: start")
      csv().fromString(text).then(processData)  /* asynchronous */
      console.log("csv: end")
    })
    console.log("makeplot: end")
  
};


function processData(data) {
  console.log("processData: start")
  let x = [], y = []

  for (let i=0; i<data.length; i++) {
      row = data[i];
      x.push( row['Date'] );
      y.push( row['Close'] );
  } 
  makePlotly( x, y );
  console.log("processData: end")
}

function makePlotly( x, y ){
  console.log("makePlotly: start")
  var traces = [{
        x: x,
        y: y
  }];
  var layout  = { title: $("#stock-select option:selected").text() + "Stock Price History"}

  myDiv = document.getElementById('myDiv');
  Plotly.newPlot( myDiv, traces, layout );
  console.log("makePlotly: end")
};