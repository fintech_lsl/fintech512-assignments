import plotly.graph_objs as go
import plotly.express as px
import sqlite3
import requests
def scatter_plot():
    conn = sqlite3.connect("stock_tracker.db")
    c = conn.cursor()
    c.execute('SELECT tracking_price, symbol FROM stock_tracker')
    rows = c.fetchall()
    conn.close()

    fig = go.Figure()
    added_symbols = set()

    for row in rows:
        symbol = row[1]
        if symbol not in added_symbols:
            stock_info1 = get_stock_info(symbol)
            if stock_info1:
                current_price = float(stock_info1["Global Quote"]["05. price"])
                fig.add_trace(
                    go.Scatter(x=[symbol], y=[current_price], mode='markers', name=symbol+"current price")
                )
            added_symbols.add(symbol)
    for row in rows:
        tracking_price = row[0]
        symbol = row[1]
        fig.add_trace(
                go.Scatter(x=[symbol], y=[tracking_price], mode='markers', name=symbol)
            )
    fig.update_layout(
        width=600, 
        height=400, 
        margin=dict(l=40, r=40, t=60, b=40), 
        title="Stock Tracker",
        xaxis_title="Stock Symbol",
        yaxis_title="Tracking Price"
    )

    return fig


def pie_chart():
    conn = sqlite3.connect("stock_tracker.db")
    c = conn.cursor()
    c.execute('SELECT SUM(shares), symbol FROM stock_tracker GROUP BY symbol')
    rows = c.fetchall()
    conn.close()

    fig = px.pie(
        values=[row[0] for row in rows],
        names=[row[1] for row in rows]
    )

    fig.update_layout(
        width=600, 
        height=400, 
        margin=dict(l=40, r=40, t=60, b=40), title="Share Distribution"
    )

    return fig

def get_stock_info(symbol):
    url = f"https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol={symbol}&apikey=config.api_key"
    response = requests.get(url)
    if response.status_code == 200:
        return response.json()
    else:
        return None