from flask import Flask, render_template, request, redirect
import sqlite3
import requests
from plotly_charts import scatter_plot, pie_chart
from datetime import datetime
from pytz import timezone, utc
from tzlocal import get_localzone

app = Flask(__name__)

# Define the datetimeformat filter
@app.template_filter('datetimeformat')
def datetimeformat(value, format='%Y-%m-%d %H:%M:%S %Z'):
    tz = timezone(str(get_localzone())) # Use the local time zone
    local_time = datetime.strptime(value, '%Y-%m-%d %H:%M:%S').replace(tzinfo=utc).astimezone(tz)
    return local_time.strftime(format)

def add_stock(symbol, tracking_price, shares):
    stock_info = get_stock_info(symbol)
    if stock_info:
        current_price = float(stock_info["Global Quote"]["05. price"])
        percent_change = round((current_price - tracking_price) / tracking_price * 100,2)
        conn = sqlite3.connect("stock_tracker.db")
        c = conn.cursor()
        c.execute("INSERT INTO stock_tracker (symbol, tracking_price, shares, current_price, percent_change) VALUES (?,?,?,?,?)", (symbol, tracking_price, shares, current_price, percent_change))
        conn.commit()
        conn.close()
        return True
    else:
        return False

def delete_stock(id):
    conn = sqlite3.connect("stock_tracker.db")
    c = conn.cursor()
    c.execute("DELETE FROM stock_tracker WHERE id=?", (id,))
    conn.commit()
    conn.close()

def get_stock_info(symbol):
    url = f"https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol={symbol}&apikey=config.api_key"
    response = requests.get(url)
    if response.status_code == 200:
        return response.json()
    else:
        return None

@app.route("/", methods=["GET", "POST"])
def index():
    if request.method == "POST":
        if "symbol" in request.form:
            symbol = request.form["symbol"]
            tracking_price = float(request.form["tracking_price"])
            shares = int(request.form["shares"])
            try:
                if add_stock(symbol, tracking_price, shares):
                    return redirect("/")
            except Exception as e:
                return "Error: Failed to add stock: {}".format(str(e))
        elif "id" in request.form:
            id = request.form["id"]
            delete_stock(id)
            return redirect("/")

        tracking_price = float(request.form["tracking_price"])
        shares = int(request.form["shares"])
        try:
            if add_stock(symbol, tracking_price, shares):
                return redirect("/")
        except Exception as e:
            return "Error: Failed to add stock: {}".format(str(e))
    else:
        conn = sqlite3.connect("stock_tracker.db")
        c = conn.cursor()
        c.execute("SELECT * FROM stock_tracker")
        stocks = c.fetchall()
        conn.close()
        
        # Call the scatter_plot() and pie_chart() functions to generate the Plotly charts
        scatter_fig = scatter_plot()
        pie_fig = pie_chart()
        
        # Convert the Plotly chart objects to HTML format
        scatter_html = scatter_fig.to_html(full_html=False)
        pie_html = pie_fig.to_html(full_html=False)
        
        return render_template("index.html", stocks=stocks, scatter_html=scatter_html, pie_html=pie_html)

if __name__ == "__main__":
    app.run(debug=True)
