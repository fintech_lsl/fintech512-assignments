import sqlite3

conn = sqlite3.connect("stock_tracker.db")
c = conn.cursor()
c.execute("CREATE TABLE IF NOT EXISTS stock_tracker (id INTEGER PRIMARY KEY AUTOINCREMENT, symbol TEXT, date_added TEXT DEFAULT CURRENT_TIMESTAMP, tracking_price REAL, shares INTEGER, current_price REAL, percent_change REAL)")
conn.commit()
conn.close()

