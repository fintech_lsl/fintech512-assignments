var my_js_var = document.getElementById("my-div").dataset.myVar;
$.ajax({
  url: '/get_stock_data',
  method: 'POST',
  data: { symbol: my_js_var },
  success: function(data) {
    console.log("Data loaded successfully:", data);var dates = data.dates;
    var prices = data.prices;
    var symbol = data.symbol;
    var trace1 = {
      type: "scatter",
      mode: "lines",
      x: dates,
      y: prices,
      name: symbol,
      line: { color: "#17BECF" }
    };
    
    var layout = {
      title: symbol,
      xaxis: { title: "Date" },
      yaxis: { title: "Closing Price" }
    };
    
    var data = [trace1];
    myDiv = document.getElementById('myDiv');
    Plotly.newPlot(myDiv, data, layout);
  }
});
