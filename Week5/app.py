from flask import Flask, render_template, request, redirect, url_for
import json
from datetime import datetime, timedelta

import requests
import config
from flask import jsonify
app = Flask(__name__)

@app.route('/')
def home():
    return render_template('home.html')
    
def get_stock_prices(symbol, api_key):
    prices_url = f'https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol={symbol}&outputsize=full&apikey={api_key}'
    response = requests.get(prices_url)
    data = response.json()
    prices = []
    dates = []
    for date, values in list(data['Time Series (Daily)'].items())[:250]:
        prices.append(float(values['4. close']))
        dates.append(date)

    return dates, prices





@app.route('/stock', methods=['GET', 'POST'])
def stock():
    if request.method == 'POST':
        symbol = request.form['symbol']
        api_key = config.api_key
        stock_data = {}

        # Get stock overview data
        overview_url = f'https://www.alphavantage.co/query?function=OVERVIEW&symbol={symbol}&apikey={api_key}'
        response = requests.get(overview_url)
        stock_data['overview'] = json.loads(response.text)

        # Get stock quote data
        quote_url = f'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol={symbol}&apikey={api_key}'
        response = requests.get(quote_url)
        stock_data['quote'] = json.loads(response.text)

        # Get stock news data
        news_data = requests.get(f'https://www.alphavantage.co/query?function=NEWS_SENTIMENT&tickers={symbol}&sort=LATEST&apikey={api_key}').json()
        stock_data['news'] = news_data['feed'][:5]
        return render_template('stock.html', stock=stock_data, symbol = symbol)
    else:
        return redirect(url_for('home'))


@app.route('/get_stock_data', methods=['GET', 'POST'])
def get_stock_data():
    if request.method == "POST":
        symbol = request.form['symbol']
        api_key = config.api_key
        dates, prices = get_stock_prices(symbol, api_key)
        data = {"dates":dates, "prices":prices, "symbol": symbol}
        return jsonify(data)
    else:
        return jsonify(error="Invalid request method")

if __name__ == '__main__':
    app.run(debug=True)

